import React from "react";

const SwapiList = ({
  results,
  next,
  previous,
  actualPage,
  getData,
  onPrevious,
  onNext,
}) => {
  return (
    <>
      <table>
        <tbody>
          {results.map(({ name, url }) => (
            <tr key={url}>
              <td>{name}</td>
            </tr>
          ))}
        </tbody>
      </table>
      <button onClick={onPrevious} disabled={!previous}>
        Prev
      </button>
      <p>{actualPage}</p>
      <button onClick={onNext} disabled={!next}>
        Next
      </button>
    </>
  );
};

export default SwapiList;
