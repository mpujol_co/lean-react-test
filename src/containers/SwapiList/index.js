import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { actionCreators as accountActions } from "../../redux/account/actions";
import { getLocalstorage, setLocalstorage } from "../../services";
import Layout from "./Layout";

const SwapiList = ({ data, loading, getData }) => {
  const [page, setPage] = useState(Number(getLocalstorage("PAGE")));

  useEffect(() => {
    getData(page || 1);
  }, [getData, page]);

  const onPrevious = () => {
    setLocalstorage(page - 1);
    setPage(page - 1);
  };
  const onNext = () => {
    setLocalstorage(page + 1);
    setPage(page + 1);
  };

  return loading || !data ? (
    <p>Cargando ...</p>
  ) : (
    <Layout
      results={data.results}
      next={data.next}
      previous={data.previous}
      actualPage={page}
      getData={getData}
      onPrevious={onPrevious}
      onNext={onNext}
    />
  );
};

const mapStateToProps = (state) => ({
  data: state.account.data,
  loading: state.account.dataLoading,
});

const mapDispatchToProps = (dispatch) => ({
  getData: (page) => dispatch(accountActions.get(page)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SwapiList);
