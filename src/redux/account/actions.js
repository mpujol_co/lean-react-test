import { completeTypes, createTypes, withPostSuccess } from "redux-recompose";
import { getData, setLocalstorage } from "../../services";
import { TARGET, TYPES } from "./constants";

export const completedTypes = completeTypes(TYPES);
export const actions = createTypes(completedTypes, "@@ACCOUNT");

export const actionCreators = {
  get: (page) => {
    return {
      type: actions.GET,
      target: TARGET.data,
      service: getData,
      payload: page,
      injections: [withPostSuccess(() => setLocalstorage("PAGE", page))],
    };
  },
};
