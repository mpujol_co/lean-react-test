export const TARGET = {
  data: "data",
};

export const TYPES = ["GET"];

export const INITIAL_STATE = {
  [TARGET.data]: null,
};
