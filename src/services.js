import { create } from "apisauce";

// define the api
const api = create({
  baseURL: "http://swapi.dev/api",
});

export const getData = (page) => api.get("/people", { page });

export const getLocalstorage = (key) => localStorage.getItem(key);
export const setLocalstorage = (key, value) => localStorage.setItem(key, value);
